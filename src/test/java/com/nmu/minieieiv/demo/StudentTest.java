package com.nmu.minieieiv.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * Created by ominieiev on 27.09.2016.
 */
public class StudentTest {

    @org.junit.Test
    public void testMeetMe() throws Exception {
        Student student = new Student();
        student.setName("Masa");
        //assertEquals(Student.MEET_ME + "Masha", student.meetMe());
        assertFalse(student.getName() == "Masa");
    }
}