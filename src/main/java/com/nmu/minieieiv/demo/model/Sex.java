package com.nmu.minieieiv.demo.model;

/**
 * Created by ominieiev on 21.11.2016.
 */
public enum Sex {
    MALE, FEMALE;
}
