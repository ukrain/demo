package com.nmu.minieieiv.demo;

/**
 * Created by ominieiev on 27.09.2016.
 */
public  class Mat {
    public static final Double PI_FROM_NMU = 3.14;

    public static Double sin(Double x) {
        return x * 2;
    }

    public Double cos(Double x) {
        return x * 3;
    }
}
